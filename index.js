const express = require('express');
const http = require('http');
const morgan = require('morgan');
const bodyParser = require('body-parser');

const dishRouter = require('./api/dishes');
const leaderRouter = require('./api/leaders');
const promotionRouter = require('./api/promotions');

const PORT = 3000;
const app = express();

app.use(morgan('dev'));

app.use(express.static(__dirname + '/public'));

app.use(bodyParser.json());

app.use('/dishes', dishRouter);
app.use('/leaders', leaderRouter);
app.use('/promotions', promotionRouter);

app.listen(PORT, ()=> {
    console.log(`Server started at ${PORT}`);
})