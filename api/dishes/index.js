const router =  require('express').Router();

router.all('/', (req, res, next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})

router.get('/', (req, res) => {
    res.status(200).end('Will send all the dishes to you!');
})

router.post('/', (req, res) => {
    res.status(201).end(`Will add the dish: ${req.body.name} 
    with details: ${req.body.description}`);
})

router.put('/', (req, res) => {
    res.status(403).end(`PUT operation not supported on /dishes`);
})
router.delete('/', (req, res) => {
    res.end('Deleting all the dishes!');
})

// ---------------------------------

router.get('/:dishId', (req, res) => {
    res.status(200).end(`Will send details of the dish: ${req.params.dishId} to you!`);
})

router.post('/:dishId', (req, res) => {
    res.status(403).end(`POST operation not supported on /dishes/${req.params.dishId}`);
})

router.put('/:dishId', (req, res) => {
    res.write(`Updating the dish: ${req.params.dishId} \n`);
    res.end(`Will update the dish: ${req.params.dishId} 
    with details: ${req.body.description}`);

})
router.delete('/:dishId', (req, res) => {
    res.end(`Deleting dish: ${req.params.dishId}`);
})


module.exports = router;