const router = require('express').Router();

router.all('/', (req, res, next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})

router.get('/', (req, res) => {
    res.status(200).end('Will send all the Leaders to you!');
})

router.post('/', (req, res) => {
    res.status(201).end(`Will add the Leader: ${req.body.name} 
    with details: ${req.body.description}`);
})

router.put('/', (req, res) => {
    res.status(403).end(`PUT operation not supported on /leaders`);
})
router.delete('/', (req, res) => {
    res.end('Deleting all the leaders!');
})

// ---------------------------------

router.get('/:leaderId', (req, res) => {
    res.status(200).end(`Will send details of the leader: ${req.params.leaderId} to you!`);
})

router.post('/:leaderId', (req, res) => {
    res.status(403).end(`POST operation not supported on /leaders/${req.params.leaderId}`);
})

router.put('/:leaderId', (req, res) => {
    res.write(`Updating the leader: ${req.params.leaderId} \n`);
    res.end(`Will update the leader: ${req.params.leaderId} 
    with details: ${req.body.description}`);

})
router.delete('/:leaderId', (req, res) => {
    res.end(`Deleting leader: ${req.params.leaderId}`);
})


module.exports = router;