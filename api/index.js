const router = require('express').Router();
const dishRouter =  require('./dishes');
const leaderRouter = require('./leaders');
const promotionRouter = require('./promotions');

router.use('/dishes', dishRouter);
router.use('/leaders', leaderRouter);
router.use('/promotions', promotionRouter);

module.exports = router;