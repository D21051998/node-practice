const router = require('express').Router();

router.all('/', (req, res, next) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    next();
})

router.get('/', (req, res) => {
    res.status(200).end('Will send all the promotions to you!');
})

router.post('/', (req, res) => {
    res.status(201).end(`Will add the promotion: ${req.body.name} 
    with details: ${req.body.description}`);
})

router.put('/', (req, res) => {
    res.status(403).end(`PUT operation not supported on /promotions`);
})
router.delete('/', (req, res) => {
    res.end('Deleting all the promotions!');
})

// ---------------------------------

router.get('/:promotionId', (req, res) => {
    res.status(200).end(`Will send details of the promotion: ${req.params.promotionId} to you!`);
})

router.post('/:promotionId', (req, res) => {
    res.status(403).end(`POST operation not supported on /promotions/${req.params.promotionId}`);
})

router.put('/:promotionId', (req, res) => {
    res.write(`Updating the promotion: ${req.params.promotionId} \n`);
    res.end(`Will update the promotion: ${req.params.promotionId} 
    with details: ${req.body.description}`);

})
router.delete('/:promotionId', (req, res) => {
    res.end(`Deleting promotion: ${req.params.promotionId}`);
})


module.exports = router;